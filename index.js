const express = require('express');
const { Client } = require('pg');

const app = express();

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Welcome to the api-security module');
});

app.post('/', (req, res) => {
  const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'API_SECURITY',
    password: 'password',
    port: 5432,
  });
  client.connect();

  // build query
  const query =
    'SELECT * FROM "IncidentReports" WHERE id=' + req.body.targetID + ';';

  client.query(query, (err, result) => {
    if (err) {
      res.status(500).send('Query Error Occurred');
    } else {
      let dataResult;
      if (Array.isArray(result)) {
        dataResult = result.map((singleResult) => singleResult.rows);
      } else {
        dataResult = result.rows;
      }

      res.send(dataResult);
    }
    client.end();
  });
});

app.listen(3000, () => {
  console.log('App listening on port 3000');
});
